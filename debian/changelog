phlipple (0.8.5-5) unstable; urgency=medium

  * Team upload.

  [ Reiner Herrmann ]
  * Fix FTBFS with GCC 10 (Closes: #957680)
  * Mark phlipple-data as Multi-Arch: foreign

  [ Markus Koschany ]
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.

 -- Markus Koschany <apo@debian.org>  Sun, 26 Jul 2020 16:30:10 +0200

phlipple (0.8.5-4) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.2.0.
  * Link against -libm explicitly.
    Thanks to Steve Langasek for the report and patch. (Closes: #907207)

 -- Markus Koschany <apo@debian.org>  Sat, 25 Aug 2018 00:50:28 +0200

phlipple (0.8.5-3) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 11.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 4.1.4.
  * Move the package to Git and salsa.debian.org.
  * Drop phlipple-dbg and use the automatic -dbgsym package instead.
  * phlipple-data: Only suggest phlipple.
  * Drop deprecated phlipple.menu file.
  * Switch to copyright format 1.0.
  * Simplify debian/rules and use dh sequencer.

 -- Markus Koschany <apo@debian.org>  Sun, 20 May 2018 23:40:34 +0200

phlipple (0.8.5-2) unstable; urgency=medium

  [ Peter Pentchev ]
  * Team upload.
  * Add the libm patch to link with -lm for cos(3).  Closes: #768741

  [ Miriam Ruiz ]
  * Upgraded Standards-Version from 3.9.4 to 3.9.6
  * Added Peter Pentchev <roam@ringlet.net> to Uploaders.

 -- Peter Pentchev <roam@ringlet.net>  Mon, 10 Nov 2014 11:23:55 +0200

phlipple (0.8.5-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New upstream release.
  * Upgraded compat level to 9.
  * Using hardening options in the compilation.
  * http://phuzzboxmedia.com/games/phlipple returns 404. Replacing it.

 -- Miriam Ruiz <miriam@debian.org>  Wed, 14 Aug 2013 01:09:28 +0200

phlipple (0.8.2-2.1) unstable; urgency=low

  [ Miriam Ruiz ]
  * Replaced Android and iOS "Support Us" buttons with a single button,
    without explicit trademarks. Closes: #649341

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Anton Balashov ]
  * Added watch file. Closes: #676447
  * Standards-Version is updated from 3.9.2 to 3.9.4

  [ Markus Koschany ]
  * debian/control: Remove superfluous ${shlibs:Depends} variables.

 -- Anton Balashov <sicness@darklogic.ru>  Thu, 08 Aug 2013 19:11:51 +0400

phlipple (0.8.2-1) unstable; urgency=low

  * Initial release. Closes: #648909

 -- Miriam Ruiz <little_miry@yahoo.es>  Wed, 16 Nov 2011 02:07:38 +0100
